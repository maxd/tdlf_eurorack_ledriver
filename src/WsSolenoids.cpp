#include "WsSolenoids.h"

namespace {
    PinPulser pulses[MAX_PULSE_COUNT];
    BoolParam enableSolenoidsParam;
    // IntParam noteIdParams[MAX_PULSE_COUNT];
    const char * noteIdAddr[MAX_PULSE_COUNT] = {
        "/tdlf/sole/1/note",
        "/tdlf/sole/2/note",
        "/tdlf/sole/3/note",
        "/tdlf/sole/4/note",
        "/tdlf/sole/5/note",
        "/tdlf/sole/6/note",
        "/tdlf/sole/7/note",
        "/tdlf/sole/8/note"
    };
    const char * pulseMsAddr[MAX_PULSE_COUNT] = {
        "/tdlf/sole/1/pul_ms",
        "/tdlf/sole/2/pul_ms",
        "/tdlf/sole/3/pul_ms",
        "/tdlf/sole/4/pul_ms",
        "/tdlf/sole/5/pul_ms",
        "/tdlf/sole/6/pul_ms",
        "/tdlf/sole/7/pul_ms",
        "/tdlf/sole/8/pul_ms"
    };
    const char * triggerAddr[MAX_PULSE_COUNT] = {
        "/tdlf/sole/1/trig",
        "/tdlf/sole/2/trig",
        "/tdlf/sole/3/trig",
        "/tdlf/sole/4/trig",
        "/tdlf/sole/5/trig",
        "/tdlf/sole/6/trig",
        "/tdlf/sole/7/trig",
        "/tdlf/sole/8/trig"
    };
}

void solenoids_setup_params(){
    enableSolenoidsParam.set("/tdlf/sole/enable", true);
    paramCollector.add(&enableSolenoidsParam);
    for(int i = 0 ; i < MAX_PULSE_COUNT; i++){
        pulses[i].noteId.set((ParamAddress)noteIdAddr[i], 0, 127, 42);
        pulses[i].noteId.inputType = NUMBER;
        pulses[i].noteId.saveType = SAVE_ON_REQUEST;
        paramCollector.add(&pulses[i].noteId);

        pulses[i].pulseLength.set((ParamAddress)pulseMsAddr[i], 0, 50, 20);
        // pulses[i].pulseLength.inputType = NUMBER;
        pulses[i].pulseLength.saveType = SAVE_ON_REQUEST;
        paramCollector.add(&pulses[i].pulseLength);

        pulses[i].trigger.set((ParamAddress)triggerAddr[i]);
        paramCollector.add(&pulses[i].trigger);
    }
}

void midi_input(uint8_t _chan, uint8_t _pitch, uint8_t _vel){
    if(_pitch != 0){
        // if(pitch == 36) Serial.print("-----");
        // Serial.printf("[midi] noteOn chan:%03i pitch:%03i vel:%03i\n",channel,pitch,velocity);
        for(int i = 0 ; i < MAX_PULSE_COUNT; i++){
            if(_pitch == pulses[i].noteId.v){
                Serial.printf("[tdlf] trigger %i\n", i);
                pulses[i].triggerStamp = millis();
            }
        }
    }
}


void update_pulses(CRGB * leds){
    for(int i = 0; i < MAX_PULSE_COUNT; i++){
        if(pulses[i].trigger.checkFlag()){
            pulses[i].triggerStamp = millis();
        }
        if(pulses[i].triggerStamp + pulses[i].pulseLength.v  > millis() ){
            if(enableSolenoidsParam.v){
                leds[i].r = 255;
            }
            else {
                leds[i].r = 0;
            }
            leds[i].g = 255;
            leds[i].b = 255;

        }
        else {
            leds[i].r = 0;
            leds[i].g = 0;
            leds[i].b = 0;
        }
    }

}


