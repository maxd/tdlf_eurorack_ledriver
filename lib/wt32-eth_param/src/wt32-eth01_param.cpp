#include "wt32-eth01_param.h"

// enable spiffs
#define WT32_PARAM_ENABLE_SPIFFS
#define FORMAT_SPIFFS_IF_FAILED true

// public
ParamCollector paramCollector;
// works on eth also
WiFiUDP oscUdpSocket;
IPAddress oscRemoteIP;
wt32_params_t wt32Params;
bool oscInputFlag;

IPAddress ethIp, ethGateway, ethSubnet;
IPAddress wifiIp, wifiGateway, wifiSubnet;
// static bool eth_connected = false;
void WiFiEvent(WiFiEvent_t event){
    switch (event) {
        // eth
        case ARDUINO_EVENT_ETH_START:
            Serial.println("[eth] Started");
            //set eth hostname here
            ETH.setHostname("esp32-ethernet");
            break;
        case ARDUINO_EVENT_ETH_CONNECTED:
            Serial.println("[eth] Connected");
            break;
        case ARDUINO_EVENT_ETH_GOT_IP:
            Serial.print("[eth] MAC: ");
            Serial.print(ETH.macAddress());
            Serial.print(", IPv4: ");
            Serial.print(ETH.localIP());
            if (ETH.fullDuplex()) {
                Serial.print(", FULL_DUPLEX");
            }
            Serial.print(", ");
            Serial.print(ETH.linkSpeed());
            Serial.println("Mbps");
            // eth_connected = true;
            break;
        case ARDUINO_EVENT_ETH_DISCONNECTED:
            Serial.println("[eth] Disconnected");
            // eth_connected = false;
            break;
        case ARDUINO_EVENT_ETH_STOP:
            Serial.println("[eth] Stopped");
            // eth_connected = false;
            break;

        // // wifi
        case ARDUINO_EVENT_WIFI_STA_START:
            Serial.println("[wifi] Started");
            //set eth hostname here
            // WIFI.setHostname("esp32-ethernet");
            break;
        case ARDUINO_EVENT_WIFI_STA_CONNECTED:
            Serial.println("[wifi] Connected");
            break;
        case ARDUINO_EVENT_WIFI_STA_GOT_IP:
            Serial.print("[wifi] MAC: ");
            Serial.print(WiFi.macAddress());
            Serial.print(", IPv4: ");
            Serial.println(WiFi.localIP());
            break;
        case ARDUINO_EVENT_WIFI_STA_DISCONNECTED:
            Serial.println("[wifi] Disconnected");
            break;
        case ARDUINO_EVENT_WIFI_STA_STOP:
            Serial.println("[wifi] Stopped");
            break;

        default:
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
/// spiffs save and load params
///

const char * configFileName = "/config.jso";
// save
void saveParamsSpiffs(){
#ifdef WT32_PARAM_ENABLE_SPIFFS
    Serial.printf("[params] saving %s\n", configFileName);
    if(SPIFFS.exists(configFileName)){
        SPIFFS.remove(configFileName);
    }
    File _file = SPIFFS.open(configFileName, "w");
    if(_file){
        // ParamJson has a StaticJsonDocument for us to use
        saveParams(paramJsonDoc, paramCollector);
        serializeJson(paramJsonDoc, _file);
        _file.close();
        // serializeJsonPretty(paramJsonDoc, Serial);
        Serial.println("\n[params] saved!");
    }
    else {
        Serial.println("[params] failed save params");
    }
#else
    Serial.printf("[params] spiffs not enabled");
#endif
}

// load
void loadParamsSpiffs(){
#ifdef WT32_PARAM_ENABLE_SPIFFS
    File _file = SPIFFS.open(configFileName, "r");
    if(_file){
        DeserializationError err = deserializeJson(paramJsonDoc, _file);
        if(err){
            Serial.printf("[params] json fail :%s \n", err.c_str());
        }
        else {
            loadParams(paramJsonDoc, paramCollector);
        }
        _file.close();
    }
    else {
        Serial.println("[params] failed load params");
    }
#else
    Serial.printf("[params] spiffs not enabled");
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////
/// osc
///

void checkOSC(){
    size_t size = oscUdpSocket.parsePacket();
    oscInputFlag = false;
    if(size > 0){
        // check for mess
        // Serial.println("[udp] lets go");
        if(oscUdpSocket.peek() == '#'){
            OSCBundle bundleIn;
            while(size--){
                bundleIn.fill(oscUdpSocket.read());
            }
            if(!bundleIn.hasError()){
                // Serial.println("[osc] bundle good");
                oscRemoteIP = oscUdpSocket.remoteIP();
                // call the param dispatch osc
                oscInputFlag = true;
                paramDispatchOSC(&bundleIn, paramCollector);
            }
            else {
                Serial.println("[osc] bundle error");
            }
        }
        else {
            OSCMessage messIn;
            while(size--){
                messIn.fill(oscUdpSocket.read());
            }
            if(!messIn.hasError()){
                // Serial.println("[osc] message good");
                oscRemoteIP = oscUdpSocket.remoteIP();
                // call the param dispatch osc
                oscInputFlag = true;
                paramDispatchOSC(&messIn, paramCollector);
            }
            else {
                Serial.println("[osc] message error");
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
/// main stuff
///

// exposed
void startNetwork(){
    Serial.println("[wt32] starting network");
    // ommiting dns for now
    // IPAddress device_dns(0,0,0,0);

    // wifi on event is for eth and wifi
    WiFi.onEvent(WiFiEvent);
    // it looks like config should be before begin, but that was crashing
    if(wt32Params.ethEnableParam.v){
        ethIp.fromString(wt32Params.ethIpParam.v);
        ethGateway.fromString(wt32Params.ethGatewayParam.v);
        ethSubnet.fromString(wt32Params.ethSubnetParam.v);
        ETH.begin();
        ETH.config(
            ethIp, 
            ethGateway,
            ethSubnet//,
            // device_dns,
            // device_dns
        );
    }
    
    if(wt32Params.wifiEnableParam.v){
        wifiIp.fromString(wt32Params.wifiIpParam.v);
        wifiGateway.fromString(wt32Params.wifiGatewayParam.v);
        wifiSubnet.fromString(wt32Params.wifiSubnetParam.v);

        Serial.printf("[wifi] enabled, connecting to %s\n", wt32Params.wifiSsidParam.v);
        WiFi.mode(WIFI_STA);
        WiFi.begin(wt32Params.wifiSsidParam.v, wt32Params.wifiPswdParam.v); // third argument is channel should be a param
        WiFi.config(
            wifiIp,
            wifiGateway,
            wifiSubnet
        );
        long t = millis();
        while (WiFi.status() != WL_CONNECTED) {
            Serial.print('.');
            delay(333);
            if(t+10000 < millis()) break;
        }
        Serial.print("[wifi] connected ip: ");
        Serial.println(WiFi.localIP());
    }
    
    if(wt32Params.wifiEnableParam.v || wt32Params.ethEnableParam.v){
        // OSC
        oscUdpSocket.begin(wt32Params.oscInPortParam.v);
        oscInputFlag = false;
        Serial.printf("[wt32] listening for OSC on %i\n", wt32Params.oscInPortParam.v);
        
        // OTA
        ArduinoOTA.onStart([]() {
            String type;
            if (ArduinoOTA.getCommand() == U_FLASH)
                type = "sketch";
            else // U_SPIFFS
                type = "filesystem";
            // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
            Serial.println("Start updating " + type);
        });
        ArduinoOTA.onEnd([]() {
            Serial.println("\nEnd");
        });
        ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
        });
        ArduinoOTA.onError([](ota_error_t error) {
            Serial.printf("Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
            else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
            else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
            else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
            else if (error == OTA_END_ERROR) Serial.println("End Failed");
        });

        ArduinoOTA.begin();
        paramWebsocketInit(&paramCollector);

    }
    else {
        Serial.println("[wt32] both interfaces off warning!");
    }
    Serial.println("[wt32] done starting network");
}

// exposed
void setupWt(){
    // wifi chonk
    wt32Params.wifiEnableParam.set( "/wt32/wifi/enable",  WT32_DEFAULT_WIFI_ENABLE);
    // wt32Params.wifiDhcpParam.set(   "/wt32/wifi/dhcp",    WT32_DEFAULT_WIFI_DHCP);
    wt32Params.wifiIpParam.set(     "/wt32/wifi/ip",      WT32_DEFAULT_WIFI_IP);
    wt32Params.wifiGatewayParam.set("/wt32/wifi/gateway", WT32_DEFAULT_WIFI_GATEWAY);
    wt32Params.wifiSubnetParam.set( "/wt32/wifi/subnet",  WT32_DEFAULT_WIFI_SUBNET);
    wt32Params.wifiSsidParam.set(   "/wt32/wifi/ssid",    WT32_DEFAULT_WIFI_SSID);
    wt32Params.wifiPswdParam.set(   "/wt32/wifi/pswd",    WT32_DEFAULT_WIFI_PSWD);
    wt32Params.wifiEnableParam.saveType = SAVE_ON_REQUEST;
    // wt32Params.wifiDhcpParam.saveType = SAVE_ON_REQUEST;
    wt32Params.wifiIpParam.saveType = SAVE_ON_REQUEST;
    wt32Params.wifiGatewayParam.saveType = SAVE_ON_REQUEST;
    wt32Params.wifiSubnetParam.saveType = SAVE_ON_REQUEST;
    wt32Params.wifiSsidParam.saveType = SAVE_ON_REQUEST;
    wt32Params.wifiPswdParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&wt32Params.wifiEnableParam);
    // paramCollector.add(&wt32Params.wifiDhcpParam);
    paramCollector.add(&wt32Params.wifiIpParam);
    paramCollector.add(&wt32Params.wifiGatewayParam);
    paramCollector.add(&wt32Params.wifiSubnetParam);
    paramCollector.add(&wt32Params.wifiSsidParam);
    paramCollector.add(&wt32Params.wifiPswdParam);
    // eth chonk
    wt32Params.ethEnableParam.set( "/wt32/eth/enable",  WT32_DEFAULT_ETH_ENABLE);
    // wt32Params.ethDhcpParam.set(   "/wt32/eth/DHCP",    WT32_DEFAULT_ETH_DHCP);
    wt32Params.ethIpParam.set(     "/wt32/eth/ip",      WT32_DEFAULT_ETH_IP);
    wt32Params.ethGatewayParam.set("/wt32/eth/gateway", WT32_DEFAULT_ETH_GATEWAY);
    wt32Params.ethSubnetParam.set( "/wt32/eth/subnet",  WT32_DEFAULT_ETH_SUBNET);
    wt32Params.ethEnableParam.saveType = SAVE_ON_REQUEST;
    // wt32Params.ethDhcpParam.saveType = SAVE_ON_REQUEST;
    wt32Params.ethIpParam.saveType = SAVE_ON_REQUEST;
    wt32Params.ethGatewayParam.saveType = SAVE_ON_REQUEST;
    wt32Params.ethSubnetParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&wt32Params.ethEnableParam);
    // paramCollector.add(&wt32Params.ethDhcpParam);
    paramCollector.add(&wt32Params.ethIpParam);
    paramCollector.add(&wt32Params.ethGatewayParam);
    paramCollector.add(&wt32Params.ethSubnetParam);    

    // other params 
    wt32Params.oscInPortParam.set("/wt32/osc/port/in", 0, 50000, WT32_DEFAULT_OSC_IN_PORT);
    wt32Params.oscInPortParam.saveType = SAVE_ON_REQUEST;
    wt32Params.oscInPortParam.inputType = NUMBER;

    paramCollector.add(&wt32Params.oscInPortParam);

    // device config
    wt32Params.saveParam.set("/wt32/config/save");
    wt32Params.saveParam.setCallback(saveParamsSpiffs);

    // init the spiffs file system
    // load the saves parameters from flash chip
#ifdef WT32_PARAM_ENABLE_SPIFFS
    paramCollector.add(&wt32Params.saveParam);
    Serial.println("[spiffs] starting");
    if(!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)){
        Serial.println("[spiffs] An Error has occurred while mounting SPIFFS");
    }
    else {
        Serial.println("[spiffs] done init");
        loadParamsSpiffs();
    }
#endif
    // setupNetwork();
}

// exposed
void updateWt(){
    if(wt32Params.wifiEnableParam.v || wt32Params.ethEnableParam.v){
        updateParamWebsocket();
        checkOSC();
        ArduinoOTA.handle();
    }
}


