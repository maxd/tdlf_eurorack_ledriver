#ifndef WS_SOLENOIDS_H
#define WS_SOLENOIDS_H

#include "wt32-eth01_param.h"
#include "FastLED.h"

#define MAX_PULSE_COUNT 8

typedef struct {
    IntParam noteId;
    IntParam pulseLength;
    BangParam trigger;
    // // led idx 
    // uint8_t noteId;
    // // solenoid address is exact channel
    // uint16_t solenoidAddress;
    // uint16_t solenoidColorOffset;
    // uint16_t ledAddress;
    // uint16_t ledColorOffset;
    // uint16_t pulseLength;
    CRGB col;
    long triggerStamp;
} PinPulser;

#define NOTE_ID_ADDR "/tdlf/sole/%i/note"
#define SOLE_ID_ADDR "/tdlf/sole/%i/pin"
#define PULSE_LEN_ADDR "/tdlf/sole/%i/pulse"
#define TRIG_ADDR "/tdlf/sole/%i/trig"


typedef struct {

} PinParams;

void solenoids_setup_params();
void midi_input(uint8_t _chan, uint8_t _pitch, uint8_t _vel);
void update_pulses(CRGB * leds);



#endif